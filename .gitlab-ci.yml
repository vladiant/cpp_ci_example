image: ubuntu:focal

stages:
  - configure
  - static_check
  - build
  - test
  - deploy
  - publish

get-compile-commands-job:
  stage: configure
  before_script:
    - apt update
    - apt install -y tzdata
    - apt install -y build-essential cmake
  script:
    - mkdir build_commands
    - echo "Getting compile commands..."
    - apt update
    - apt install -y tzdata
    - apt install -y build-essential cmake
    - cd build_commands
    - cmake .. -DCMAKE_BUILD_TYPE=Debug  -DCMAKE_EXPORT_COMPILE_COMMANDS=ON
    - echo "Getting compile commands complete."
  artifacts:
    paths:
    - build_commands

cppcheck-job:
  stage: static_check
  before_script:
    - apt update
    - apt install -y tzdata
    - apt install -y build-essential cmake
  dependencies:
    - get-compile-commands-job
  script:
    - echo "Running cppcheck..."
    - apt update
    - apt install -y cppcheck
    - cd build_commands
    - cppcheck --project=compile_commands.json
               --xml-version=2 
               --output-file=cppcheck-report.txt --std=c++17 .
  artifacts:
      paths:
        - build_commands/cppcheck-report.txt

clang-tidy-job:
  stage: static_check
  before_script:
    - apt update
    - apt install -y tzdata
    - apt install -y build-essential cmake
  dependencies:
    - get-compile-commands-job
  script:
    - echo "Running clang-tidy..."
    - apt update
    - apt install -y clang-tidy
    - apt install -y clang-tools
    - cd build_commands
    - run-clang-tidy -checks='cppcoreguidelines-*,performance-*,readibility-*,
                      modernize-*,-modernize-use-trailing-return-type,misc-*,
                      clang-analyzer-*' -export-fixes clang-tidy-fixes.yaml
  artifacts:
      paths:
        - build_commands/clang-tidy-fixes.yaml

build-asan-job:
  stage: build
  before_script:
    - apt update
    - apt install -y tzdata
    - apt install -y build-essential cmake
    - apt install -y clang
  script:
    - echo "Compiling the ASAN code..."
    - mkdir build_asan
    - cd build_asan
    - cmake .. -DCMAKE_C_COMPILER=/usr/bin/clang
               -DCMAKE_CXX_COMPILER=/usr/bin/clang++
               -DCMAKE_CXX_FLAGS="-O1 -g -fsanitize=address -fno-omit-frame-pointer"
    - make
    - echo "Compile ASAN complete."
  artifacts:
    paths:
    - build_asan

build-debug-job:
  stage: build
  before_script:
    - apt update
    - apt install -y tzdata
    - apt install -y build-essential cmake
  script:
    - echo "Compiling the code..."
    - apt install -y lcov
    - mkdir build_debug
    - cd build_debug
    - cmake .. -DCMAKE_BUILD_TYPE=Debug
    - make
    - echo "Compile complete."
  artifacts:
    paths:
    - build_debug

build-release-job:
  stage: build
  before_script:
    - apt update
    - apt install -y tzdata
    - apt install -y build-essential cmake
  script:
    - echo "Compiling the code..."
    - mkdir build_release
    - cd build_release
    - cmake .. -DCMAKE_BUILD_TYPE=Release
    - make
    - echo "Compile complete."
  artifacts:
    paths:
    - build_release

unit-test-debug-job:
  stage: test
  dependencies:
    - build-debug-job
  script:
    - echo "Running unit tests..."
    - apt update
    - apt install -y tzdata
    - apt install -y build-essential cmake
    - cd build_debug
    - make test

unit-test-release-job:
  stage: test
  dependencies:
    - build-release-job
  script:
    - echo "Running unit tests..."
    - apt update
    - apt install -y tzdata
    - apt install -y build-essential cmake  
    - cd build_release
    - make test

memcheck-job:
  stage: test
  dependencies:
    - build-debug-job
  script:
    - echo "Running memcheck tests..."
    - apt update
    - apt install -y tzdata
    - apt install -y build-essential cmake
    - apt install -y valgrind
    - cd build_debug
    - cmake --build . --target valgrind
  artifacts:
    paths:
    - build_debug/memcheck.txt

valgrind-memcheck-job:
  stage: test
  dependencies:
    - build-debug-job
  script:
    - echo "Running valgrind memcheck tests..."
    - apt update
    - apt install -y tzdata
    - apt install -y build-essential cmake
    - apt install -y valgrind
    - cd build_debug
    - valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes --log-file=valgrind-memcheck.txt ./TestSampleLib
  artifacts:
    paths:
    - build_debug/valgrind-memcheck.txt

valgrind-thread-analyzer-job:
  stage: test
  dependencies:
    - build-debug-job
  script:
    - echo "Running valgrind thread analyzer tests..."
    - apt update
    - apt install -y tzdata
    - apt install -y build-essential cmake
    - apt install -y valgrind
    - cd build_debug
    - valgrind --tool=helgrind --log-file=valgrind-helgrind.txt ./TestSampleLib
  artifacts:
    paths:
    - build_debug/valgrind-helgrind.txt

unit-test-asan-job:
  stage: test
  dependencies:
    - build-asan-job
  script:
    - echo "Running ASAN unit tests..."
    - apt update
    - apt install -y tzdata
    - apt install -y build-essential cmake  
    - cd build_asan
    - ctest -VV --progress

release-package-job:
  stage: deploy
  dependencies:
    - build-release-job
  script:
    - echo "Generating package..."
    - apt update
    - apt install -y tzdata
    - apt install -y build-essential cmake
    - apt install -y file
    - cd build_release
    - make package
  artifacts:
    paths:
    - build_release/CppProjectSample-1.0.0-Linux.deb


cppcheck-report-job:
  stage: deploy
  before_script:
    - apt update
    - apt install -y tzdata
    - apt install -y build-essential cmake
  dependencies:
    - cppcheck-job
  script:
    - echo "Running cppcheck report..."
    - apt update
    - apt install -y python3-pip
    - pip install cppcheck-junit
    - cppcheck_junit build_commands/cppcheck-report.txt cppcheck-report.xml
  artifacts:
      paths:
        - cppcheck-report.xml
      reports:
        junit: cppcheck-report.xml

unit-test-debug-report-job:
  stage: deploy
  dependencies:
    - build-debug-job
  script:
    - echo "Preparing release unit tests report..."
    - cd build_debug
    - ./TestSampleLib --reporter junit --out=test_results_debug.xml
  artifacts:
    paths:
    - build_debug/test_results_debug.xml
    reports:
      junit: build_debug/test_results_debug.xml

unit-test-release-report-job:
  stage: deploy
  dependencies:
    - build-release-job
  script:
    - echo "Preparing release unit tests report..."
    - cd build_release
    - ./TestSampleLib --reporter junit --out=test_results_release.xml
  artifacts:
    paths:
    - build_release/test_results_release.xml
    reports:
      junit: build_release/test_results_release.xml

coverage-report-job:
  stage: deploy
  when: always
  before_script:
    - apt update
    - apt install -y tzdata
    - apt install -y build-essential cmake
  dependencies:
    - build-debug-job
  script:
    - echo "Preparing coverage report..."
    - apt install -y lcov
    - cd build_debug
    - make cov
  artifacts:
      paths:
        - build_debug/cov/

pages:
  stage: publish
  dependencies:
    - coverage-report-job
  script:
  - mv build_debug/cov/ public/
  artifacts:
    paths:
    - public
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
